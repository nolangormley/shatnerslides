# Shatner Slides
## Running
1. Ensure that you have python 3.x installed
2. Run with ``` `which python3` server.py```
3. Point a browser to http://localhost:8000
## Usage
- Use the spacebar to navigate through cues
- 'f' key will request full screen, 'esc' to exit
- Keys 'q' and 'a' will raise and lower the volume respectively

## More info and Demo 
Demo not active yet
[Link](https://gormley.dev/blog/shatner-slides/)
