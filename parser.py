import random
import json

def main():
    file = open('./sample_input', 'r')
    lines = file.readlines()
    data = []
    opener = False
    for line in lines:
        tokens = line.strip().split(' ')
        if(tokens[0] == 'opener'):
            # Add opening sequence
            opener = True
            if(len(tokens) != 3):
                raise Exception('Wrong number of arguments in line: %s', line)
            else:
                data.insert(0, {"song":"/mus/{}.mp3".format(tokens[1]), "continue":False, "slide":"/img/galaxy.jpg", "fade":False, "wait":tokens[2]})
        elif(tokens[0] == 'closer'):
            # Add closing sequence
            if(len(tokens) != 2):
                raise Exception('Wrong number of arguments in line: %s', line)
            else:
                if(opener):
                    opener = False
                    data.insert(0, {"song":False, "continue":True, "slide":False, "fade":True, "wait":False})
                else:
                    data.insert(0, {"song":"/mus/{}.mp3".format(tokens[1]), "continue":False, "slide":"/img/galaxy.jpg", "fade":False, "wait":False})
                data.insert(0, {"song":False, "continue":False, "slide":"/img/galaxy.jpg", "fade":False, "wait":False})
        elif(tokens[0] == '-'):
            # Add music cue
            if(len(tokens) != 2):
                raise Exception('Wrong number of arguments in line: %s', line)
            else:
                data.insert(0, {"song":"/mus/{}.mp3".format(tokens[1]), "continue":False, "slide":False, "fade":False, "wait":False})
                data.insert(0, {"song":False, "continue":False, "slide":False, "fade":False, "wait":False})
                
        else:
            # Add slide sequence

            # Random Song
            if(len(tokens) == 1):
                randomNum = random.randint(1,4)

                if(opener):
                    opener = False
                    data.insert(0, {"song":False, "continue":True, "slide":False, "fade":True, "wait":False})
                else:
                    data.insert(0, {"song":"/mus/{}.mp3".format(randomNum), "continue":False, "slide":"/img/galaxy.jpg", "fade":False, "wait":False})
                data.insert(0, {"song":False, "continue":True, "slide":"/img/{}.png".format(tokens[0]), "fade":False, "wait":False})
                data.insert(0, {"song":False, "continue":False, "slide":False, "fade":False, "wait":False})

            # Specific Song
            elif(len(tokens) == 2):
                data.insert(0, {"song":"./static/music/{}.mp3".format(tokens[1]), "continue":False, "slide":"/img/galaxy.jpg", "fade":False, "wait":False})
                data.insert(0, {"song":False, "continue":True, "slide":"/img/{}.png".format(tokens[0]), "fade":False, "wait":False})
                data.insert(0, {"song":False, "continue":False, "slide":False, "fade":False, "wait":False})
            else:
                raise Exception('Wrong number of arguments in line: %s', line)

    with open('testdata.json', 'w') as outfile:
        json.dump(data, outfile)

main()