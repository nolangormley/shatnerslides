import cherrypy
from jinja2 import Template
from os.path import isfile, join
from os import listdir
import json
import uuid


class website(object):

    @cherrypy.expose
    def show(self, cues_filename=None):
        with open('templates/index.html') as file_:
            template = Template(file_.read())
            
        return template.render(cues_filename = cues_filename)

    @cherrypy.expose
    def index(self):
        with open('templates/create.html') as file_:
            template = Template(file_.read())
        with open('templates/navbar.html') as file_:
            navbar_raw = Template(file_.read())
        with open('templates/slide_form.html') as file_:
            slide_form = Template(file_.read())

        return template.render(navbar=navbar_raw.render(), 
            form=slide_form.render(slides = self.getSlides(), songs = self.getSongs())
        )

    @cherrypy.expose
    def getCues(self, file='static/data.json'):
        with open(file) as file_:
            data = file_.read()
        return data

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def submitSlides(self):
        reply = { 'error' : False }

        slides = []
        data = cherrypy.request.json

        # import pdb; pdb.set_trace()
        # {song:'', continue:'', slide:'', fade:'', wait:''}

        opener = False
        for slide in data:
            song = self.getSongFromName(slide['song'])
            slide_image = self.getSlideFromName(slide['slide'])
            slide_path = ""
            song_path  = ""
            if slide_image:
                slide_path = slide_image['file-name']
            if song:
                song_path = song['file-name']

            # import ipdb; ipdb.set_trace()

            if slide['type'] == 0:
                # Add opening sequence
                opener = True
                slides.insert(0, {"song":song_path, "continue":False, "slide":slide_path, "fade":False, "wait":slide['wait']})
            elif slide['type'] == 1:
                # Add closing sequence
                if opener:
                    opener = False
                    slides.insert(0, {"song":False, "continue":True, "slide":False, "fade":True, "wait":False})
                else:
                    slides.insert(0, {"song":song_path, "continue":False, "slide":"galaxy.png", "fade":False, "wait":False})
                slides.insert(0, {"song":False, "continue":False, "slide":"galaxy.png", "fade":False, "wait":False})
            elif slide['type'] == 2:
                # Add slide sequence
                if opener:
                    opener = False
                    slides.insert(0, {"song":False, "continue":True, "slide":False, "fade":True, "wait":False})
                else:
                    slides.insert(0, {"song":song_path, "continue":False, "slide":"galaxy.jpg", "fade":False, "wait":False})
                slides.insert(0, {"song":False, "continue":True, "slide":slide_path, "fade":False, "wait":False})
                slides.insert(0, {"song":False, "continue":False, "slide":False, "fade":False, "wait":False})
            else:
                # Add song sequence
                slides.insert(0, {"song":song_path, "continue":False, "slide":False, "fade":False, "wait":False})
                slides.insert(0, {"song":False, "continue":False, "slide":False, "fade":False, "wait":False})

        # slides.reverse()

        filename = 'slides/' + str(uuid.uuid4()) + '_slides.json'
        with open(filename, 'w') as outfile:
            json.dump(slides, outfile)

        reply['filename'] = filename

        return json.dumps(reply)
        

    def getSongFromName(self, name):
        songs = self.getSongs()
        song = list(filter(lambda songs: songs['name'] == name, songs))
        if len(song) is 1:
            return song.pop()
        else:
            return False

    def getSongs(self):
        with open('./static/mus/music_data.json') as file_:
            songs = json.loads(file_.read())
        return songs
    
    def getSlideFromName(self, name):
        slides = self.getSlides()
        slide = list(filter(lambda slides: slides['name'] == name, slides))
        if len(slide) is 1:
            return slide.pop()
        else:
            return False

    def getSlides(self):
        with open('./static/img/slides/slide_data.json') as file_:
            slides = json.loads(file_.read())
        return slides

cherrypy.quickstart(website(), '/', "./conf/app.conf")
