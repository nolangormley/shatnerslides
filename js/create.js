window.onload = function () {
    template = $("#hidden-template").html()
    document.getElementById("add-slide-button").addEventListener("click", function () {
        $("#slide-forms").append(template)
    });
    document.getElementById("start-show-button").addEventListener("click", function () {
        startShow()
    });
    document.getElementById("slide-forms").addEventListener("click", (event) => {
        if (event.target.nodeName === 'BUTTON') {
            $(event.target).parent().remove()
        }
    });
};

function startShow() {
    data = saveShow()

    $.ajax({
        type: "POST",
        url: "/submitSlides",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        dataType: "json",
    
        success: function (data) {
            console.log(data)
            window.location = window.location + 'show?cues_filename=' + data['filename']
        },
        failure: function(error) {
            console.log(data)
        }
    });

}

function saveShow() {
    forms = $("#slide-forms")
    children = forms.children()
    slides = []


    for (i = 0; i < children.length; i++) {
        data = {}
        data['slide'] = children[i].children[0].children[1].value
        data['song']  = children[i].children[1].children[1].value
        data['wait']  = children[i].children[2].children[1].value

        
        // Determine the type of slide, the types are as follows:
        // (0) Opener, meaning opening slide
        // (1) Closer, meaning closing slide
        // (2) Game, meaning a regular game slide
        // (3) Song, meaning it will just be a song queue
        if(children[i].children[3].children[1].value == "Opener")
            data['type'] = 0
        else if(children[i].children[3].children[1].value == "Closer")
            data['type'] = 1
        else if(children[i].children[3].children[1].value == "Game")
            data['type'] = 2
        else
            data['type'] = 3

        slides.push(data)
    }

    return slides;
}