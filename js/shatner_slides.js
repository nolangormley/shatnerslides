window.onload = function() {
    cues_filename = this.document.getElementById('hidden-filename').innerHTML
    getCues()
};

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

// cues = {song:'', continue:'', slide:'', fade:'', wait:''}
function init(cues) {

    // Setup the new Howl.
    var sound = new Howl({
        src: ['/mus/1.mp3']
    });

    var curSoundId
    var soundLevel = 1
    // Volume that fades go to
    var fadedVolume = .125

    // Watch the page for all keypress events
    document.addEventListener("keypress", function(event) {
        // 'spacebar' to execute next cue
        if (event.charCode == 32) {
            // Get next cue off the stack
            var cue = cues.pop()
            
            // If there is a new slide and there is no pre-wait time for it, show it
            if(cue.slide && !cue.wait)
                document.getElementById('slide').src = "/img/" + cue.slide
            // If the music is not to continue and there is music playing, stop it
            if(!cue.continue && sound.playing()) {
                // Fade from current volume to 0, wait for that to complete, then stop the music
                sound.fade(sound.volume(curSoundId), 0, 1000, curSoundId)
                sleep(1000).then(() => {
                    sound.stop()
                })
            }
            // If there is a new song to be played, add a new Howl and play
            if(cue.song) {
                sound = new Howl({
                    src: cue.song
                })
                // Set saved volume
                sound.volume(soundLevel)
                // Set Current Sound ID
                curSoundId = sound.play()
            }
            // If the music is to be faded and a current sound id exists, fade the music
            if(cue.fade && curSoundId) {
                sound.fade(soundLevel, fadedVolume, 1500, curSoundId)
                // faded = true
            }
            // If a prewait is given, wait the specified time and then show the slide
            if(cue.wait) {
                sleep(cue.wait).then(() => {
                    document.getElementById('slide').src = "/img/" + cue.slide
                })
            }
        }
        // Press 'f' to fullscreen
        else if (event.charCode == 102) {
            elem = document.getElementById('slide')
            openFullscreen(elem)
        }
        // Press 'q' to turn volume up
        else if (event.charCode == 113) {
            soundLevel = sound.volume()+0.05
            if(soundLevel > 1)
                soundLevel = 1
            sound.volume(soundLevel)
        }
        // Press 'a' to turn volume down
        else if (event.charCode == 97) {
            soundLevel = sound.volume()-0.05
            if(soundLevel < 0)
                soundLevel = 0
            sound.volume(soundLevel)
        }
    })
}

function openFullscreen(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function getCues() {
    $.ajax({
        type: "GET",
        url: "/getCues?file=" + cues_filename.trim(),
        contentType: "application/json; charset=utf-8",
    
        success: function (data) {
            init(JSON.parse(data))
        },
        failure: function(error) {
            console.log(data)
        }
    });
}